class InitialMigration < ActiveRecord::Migration
  def self.up
    create_table :areas, :force => true do |t|
      t.column :name, :string
      t.column :project_id, :integer
    end

    create_table :bugs, :force => true do |t|
      t.column :category, :string
      t.column :user_id, :integer
      t.column :priority_id, :integer
      t.column :project_id, :integer
      t.column :status, :integer
      t.column :estimate, :string
      t.column :title, :string
      t.column :release_id, :integer
      t.column :version, :string
      t.column :date_created, :datetime
      t.column :area_id, :integer
      t.column :open_user_id, :integer
      t.column :subscriptions, :text
      t.column :comments_count, :integer
    end
    add_index :bugs, :user_id
    add_index :bugs, :project_id
    add_index :bugs, :release_id
    add_index :bugs, :priority_id
    add_index :bugs, :area_id
    add_index :bugs, :title
    
    create_table :changes, :force => true do |t|
      t.column :user_id, :integer
      t.column :msg, :text
      t.column :date_created, :datetime
      t.column :change_type, :integer
      t.column :attachment, :string
      t.column :attachment_type, :string
      t.column :bug_id, :integer
      t.column :log, :text
    end
    add_index :changes, :user_id
    add_index :changes, :bug_id

    create_table :comments, :force => true do |t|
      t.column :bug_id, :integer
      t.column :msg, :text
      t.column :parent_id, :integer
      t.column :user_id, :integer
      t.column :date_created, :datetime
      t.column :comments_count, :integer
    end
    add_index :comments, :bug_id
    add_index :comments, :parent_id
    add_index :comments, :user_id

    create_table :filters, :force => true do |t|
      t.column :filter, :text
      t.column :user_id, :integer
      t.column :name, :string
    end
    add_index :filters, :user_id
    
    create_table :priorities, :force => true do |t|
      t.column :title, :string
    end

    create_table :projects, :force => true do |t|
      t.column :name, :string
      t.column :status, :integer
      t.column :manager_id, :integer
    end
    add_index :projects, :name, :unique => true
    add_index :projects, :manager_id

    create_table :releases, :force => true do |t|
      t.column :project_id, :integer
      t.column :name, :string
      t.column :assignable, :boolean
      t.column :rel_date, :date
    end
    add_index :releases, :project_id

    create_table :users, :force => true do |t|
      t.column :login, :string
      t.column :first_name, :string
      t.column :last_name, :string
      t.column :email, :string
      t.column :phone, :string
      t.column :status, :integer
      t.column :pwd, :string
      t.column :filter_id, :integer
      t.column :is_admin, :boolean
      t.column :is_notify, :boolean
    end
    add_index :users, :login, :unique => true
    
    # insert some data to bootstrap the app
    sql = File.read("#{RAILS_ROOT}/db/initial_data.sql")
    sql.each_line {|line| execute line }
  end

  def self.down
    drop_table :areas
    drop_table :bugs
    drop_table :changes
    drop_table :comments
    drop_table :filters
    drop_table :priorities
    drop_table :projects
    drop_table :releases
    drop_table :users
  end
end
