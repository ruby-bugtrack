require 'ostruct'

class Filter < ActiveRecord::Base
  require_dependency 'bug'

  belongs_to :user
  serialize :filter

  validates_presence_of :name
  validates_uniqueness_of :name, :scope => 'user_id'

  def self.default_filter(user_id)
    value = OpenStruct.new
    value.name = 'Assigned to me'
    value.include = ['open']
    value.estimate = 0
    value.project = -1
    value.area = -1
    value.open_by = -1
    value.assigned_to = user_id
    value.status = -1
    value.release = -1
    value.priority_operation = '='
    value.priority = -1
    value.category = '-- All Categories --'
    value.limit = 0
    value
  end

  def self.create_default(user)
    filter = Filter.new do |f|
      f.filter = default_filter(user.id)
      f.user_id = user.id
    end
    filter.save
    filter
  end

  def setup(value)
    value = OpenStruct.new value

    # setting initial values
    value.include ||= []
    value.project ||= -1
    value.estimate ||= 0
    value.area ||= -1
    value.open_by ||= -1
    value.assigned_to ||= -1
    value.status ||= -1
    value.release ||= -1
    value.priority_operation ||= '='
    value.priority ||= -1
    value.limit = 0 if value.limit.nil? || value.limit.empty?

    # converting values
    value.project = value.project.to_i
    value.area = value.area.to_i
    value.estimate = value.estimate.to_i
    value.open_by = value.open_by.to_i
    value.assigned_to = value.assigned_to.to_i
    value.status = value.status.to_i
    value.release = value.release.to_i
    value.priority = value.priority.to_i
    value.limit = value.limit.to_i
    
    self.filter = value
  end

  def filter=(value)
    write_attribute('filter', value)
    self.name = value.name
  end

  def sql
    filter = self.filter
    result = []
    result << "bugs.status <> #{Bug::CLOSED}" if filter.include.include? 'open'
    result << "bugs.status = #{Bug::CLOSED}" if filter.include.include? 'closed'
    result << "bugs.category = '#{filter.category}'" if Bug::CATEGORY.include? filter.category
    result << "bugs.estimate = ''" if filter.estimate == 1
    result << "bugs.project_id = #{filter.project}" if filter.project != -1
    result << "bugs.area_id = #{filter.area}" if filter.area != -1
    result << "bugs.open_user_id = #{filter.open_by}" if filter.open_by != -1
    result << "bugs.user_id = #{filter.assigned_to}" if filter.assigned_to != -1
    result << "bugs.status between 2 AND 7" if filter.status == -2
    result << "bugs.status = #{filter.status}" if filter.status >= 0
    result << "bugs.release_id = #{filter.release}" if filter.release >= 0
    result << "bugs.release_id IS NULL" if filter.release == -2
    result << "bugs.priority_id #{filter.priority_operation} #{filter.priority}" if filter.priority != -1

    result.join(' AND ') unless result.empty?
  end

  def limit
    "LIMIT #{self.filter.limit}" if self.filter.limit > 0
  end

  def to_hash
    {'id' => self.id, 'name' => self.name, 'sql' => self.sql, 'limit' => self.limit}
  end
end
