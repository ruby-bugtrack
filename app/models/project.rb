class Project < ActiveRecord::Base
  belongs_to  :manager, :class_name => 'User', :foreign_key => 'manager_id'
  has_many :releases, :class_name => 'Release', :exclusively_dependent => true
  has_many :areas, :exclusively_dependent => true

  validates_presence_of :name
  validates_uniqueness_of :name

  STATUS_NAMES = %w{DISABLED ACTIVE}
  DISABLED, ACTIVE = 0, 1

  def status_name
    STATUS_NAMES[self.status]
  end

  def self.find_all_with_manager
    find_by_sql %{
       SELECT projects.*, users.login as manager_name
       FROM projects, users
       WHERE projects.manager_id = users.id
    }
  end

  def active?
    self.status == ACTIVE
  end

  ######################################################################
  protected
  ######################################################################

  def before_create
    self.status = 0 unless self.status
  end

end
