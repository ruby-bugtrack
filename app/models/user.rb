class User < ActiveRecord::Base
  belongs_to :filter
  has_many   :filters, :exclusively_dependent => true, :order => 'name ASC'

  attr_protected :pwd

  validates_presence_of :login, :first_name, :last_name, :email
  validates_presence_of :pwd
  validates_uniqueness_of :login

  after_create :create_default_filter

  def full_name
    "#{self.first_name} #{self.last_name}"
  end

  alias :user_name :full_name

  STATUS_NAMES = %w{DISABLED ACTIVE}
  DISABLED, ACTIVE = 0, 1

  def status_name
    STATUS_NAMES[self.status]
  end

  def self.auth(login, pwd)
    find_first("login = '#{login}' AND pwd = '#{pwd}' AND status = #{ACTIVE}") rescue return nil
  end

  def is_admin?
    self.is_admin
  end

  def is_notify?
    self.is_notify
  end

  def active?
    self.status == ACTIVE
  end

  def create_default_filter
    self.filter = Filter.create_default(self)
    self.save
  end

  def can_edit_change(change)
    verify_access(change)
  end

  def can_edit_bug(bug)
    verify_access(bug)
  end

  def verify_access(thing)
    if self.is_admin? || thing.user_id.nil? || thing.user_id == self.id
      true
    else
      thing.errors.add_to_base 'Access denied'
      false
    end
  end
  private :verify_access


  ######################################################################
  protected
  ######################################################################

  def before_create
    self.status = 1
  end

  def self.human_attribute_name(attr)
    if attr == 'pwd'
      'Password'
    else
      super
    end
  end
end
