class Change < ActiveRecord::Base
  belongs_to :user
  serialize  :log

  def display_date_created
    self.date_created.strftime('%m/%d/%Y %I:%M %p')
  end

  OPENED_TYPE, EDITED_TYPE, RESOLVED_TYPE, ASSIGNED_TYPE, 
  REACTIVATED_TYPE, CLOSED_TYPE, REOPENED_TYPE = (0..7).to_a

  CHANGE_TYPES = %w{Opened Edited Resolved Assigned Reactivated Closed Reopened}

  def self.extended_changes(bug)
    find_by_sql [%{
      SELECT changes.*, CONCAT(users.first_name, ' ', users.last_name) user_name
      FROM changes INNER JOIN users ON (changes.user_id = users.id) 
      WHERE changes.bug_id = ?
      ORDER BY date_created ASC 
    }, bug.id]
  end

  def type_name
    CHANGE_TYPES[self.change_type]
  end

  def self.create(by_user, change_type, msg)
    new do |c|
      c.user_id = by_user
      c.msg = msg
      c.change_type = change_type
    end
  end

  def attach(attachment)
    return unless attachment.size > 0
    self.attachment = attachment.original_filename.gsub(/^.*(\\|\/)/,'')
    self.attachment_type = attachment.content_type
    
    # copy attachment
    full_dir = File.join($ATTACHMENTS, self.bug_id.to_s)
    Dir.mkdir(full_dir) unless File.exists?(full_dir)
    open(File.join(full_dir, self.attachment), 'wb') do |file|
      while buffer = attachment.read(1024)
        file << buffer
      end
    end
  end

  def attachment_path
    File.join($ATTACHMENTS, self.bug_id.to_s, self.attachment) if self.attachment
  end

  def log=(value)
    return if self.change_type == OPENED_TYPE
    write_attribute('log', value)
  end

  def each_log
    return unless self.log
    self.log.each do |name, (old, new)|
      yield "#{name} from #{old} to #{new}"
    end
  end

  def assignment_change?
    [ASSIGNED_TYPE, OPENED_TYPE].include? self.change_type
  end

  def has_message?
    self.msg && !self.msg.empty?
  end

  def empty?
    return false if has_message? 
    return false if self.log && !self.log.empty?
    return false if self.attachment
    return false unless self.change_type == EDITED_TYPE
    true
  end

  ################################################################################
  protected
  ################################################################################

  def before_create
    self.date_created = Time.now
  end

  def validate
    self.errors.add_to_base 'No changes have been made.' if self.empty?
  end
end
