class Notifications < ActionMailer::Base

  FROM = 'bugtrack@noreply.com'

  def assignment(bug, url, sent_on = Time.now)
    @recipients = bug.user.email
    @from       = FROM
    @subject    = "Task #{bug.id} has been assigned to you."
    @body['bug']  = bug
    @body['url'] = url
    @sent_on    = sent_on
  end

  def change_log(bug, users, url, sent_on = Time.now)
    @recipients = users.map{|user| user.email}
    @from       = FROM
    @subject    = "Task #{bug.id} has been changed."
    @body['bug'] = bug
    @body['url'] = url
    @sent_on    = sent_on
  end
end
