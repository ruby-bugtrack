class Bug < ActiveRecord::Base
  belongs_to :project
  belongs_to :user
  belongs_to :opened_by, :class_name => 'User', :foreign_key => 'open_user_id'
  belongs_to :priority
  belongs_to :release
  belongs_to :area

  has_many   :changes,  :exclusively_dependent => true, :order => 'date_created ASC'
  has_many   :root_comments, :class_name => "Comment", 
             :exclusively_dependent => true, :conditions => 'parent_id IS NULL', 
             :order => 'date_created'

  serialize  :subscriptions

  COMMON_SQL_FIND_PART = %{
      SELECT bugs.*, users.login user_login, 
             CONCAT(users.first_name, ' ', users.last_name) user_name, 
             priorities.title priority_title, IFNULL(releases.name,'Unspecified') release_name
      FROM (bugs, users, priorities) LEFT OUTER JOIN releases ON (bugs.release_id = releases.id)
      WHERE bugs.user_id = users.id AND bugs.priority_id = priorities.id
  }

  def self.find_all_for_listing(project, filter = nil, order_by = nil, direction = 'ASC')
    order_by ||= 'id'
    direction = 'ASC' unless %w{ASC DESC}.include? direction
    find_by_sql %{
      #{COMMON_SQL_FIND_PART}
        AND bugs.project_id = #{project.id} #{'AND ' + filter.sql if filter && filter.sql}
      ORDER BY #{order_by} #{direction} #{ filter.limit if filter }
    }
  end

  def self.find_all_full_search(project, key, search_closed = true)
    find_by_sql %{
      #{COMMON_SQL_FIND_PART}
        AND bugs.project_id = #{project.id}
        AND MATCH(bugs.title) AGAINST ('#{key}')
        #{'AND bugs.status <> 1' unless search_closed}
      ORDER BY id ASC
    }
  end

  def self.find_all_like_search(project, key, search_closed = true)
    find_by_sql %{
      #{COMMON_SQL_FIND_PART}
        AND bugs.project_id = #{project.id}
        AND bugs.title LIKE '%#{key}%'
        #{'AND bugs.status <> 1' unless search_closed}
      ORDER BY id ASC
    }
  end

  def self.find_next(id, filter = nil)
    cond = "id > #{id}"
    cond << " AND #{filter.sql}" if filter && filter.sql
    find_first cond, "id ASC"
  end

  def self.find_prev(id, filter = nil)
    cond = "id < #{id}"
    cond << " AND #{filter.sql}" if filter && filter.sql
    find_first cond, "id DESC"
  end

  def full_changes
    Change.extended_changes(self)
  end

  CATEGORY = %w{Bug Feature Inquiry}

  STATUS_NAMES = [
    'Active', 
    'Closed',
    'Resolved (Fixed)', 
    'Resolved (Not reproducible)', 
    'Resolved (Duplicate)',
    'Resolved (Postponed)',
    'Resolved (Won\'t fix)',
    'Resolved (By Design)',
  ]

  ACTIVE = 0
  CLOSED = 1
  RESOLVE_RANGE = (2..7)

  def status_name
    STATUS_NAMES[self.status]
  end

  ASSIGN_CMD, RESOLVE_CMD, REACTIVATE_CMD, CLOSE_CMD, REOPEN_CMD = (0..4).to_a
  
  def bulk_mode
    result = 0
    case self.status
    when ACTIVE
      result |= (1 << ASSIGN_CMD)
      result |= (1 << RESOLVE_CMD)
    when RESOLVE_RANGE
      result |= (1 << REACTIVATE_CMD)
      result |= (1 << CLOSE_CMD)
    when CLOSED
      result |= (1 << REOPEN_CMD)
    end
    result
  end

  def edit(by_user, params)
    return nil unless by_user.can_edit_bug(self)
    # fix params for 'Unspecified' release
    params['bug']['release_id'] = nil if params['bug']['release_id'] == '-1'

    self.open_user_id = by_user.id if is_open_change?(params)

    change = Change::create(by_user.id, params['change_type'], params['msg'])

    # convert change to assignment change in case the task was assigned to another user
    # during editing
    change.change_type = Change::ASSIGNED_TYPE if is_assignment_change?(params)

    self.attributes = params['bug']
    subscribe(self.user_id) if change.assignment_change?
    return false unless save

    self.changes << change
    change.log = @log
    change.attach(params['attachment']) 

    if change.empty?
      self.errors.add_to_base 'No changes have been made.' 
      change.destroy
      return nil
    else
      return change if change.save
    end
  end

  def subscribe(user_id)
    self.subscriptions ||= []
    self.subscriptions << user_id unless self.subscriptions.include?(user_id)
  end

  def unsubscribe(user_id)
    self.subscriptions.delete user_id
  end

  def create_comment(user_id, msg)
    comment = Comment.create(user_id, msg)
    self.root_comments << comment
    return comment if comment.save
  end

  def active?
    self.status == ACTIVE
  end

  def resolved?
    RESOLVE_RANGE.include? self.status
  end

  def has_comments?
    self.comments_count > 0
  end
  
  def display_estimate
    if self.estimate.nil? || self.estimate.empty?
      'Unspecified'
    else
      ESTIMATOR[self.estimate]
    end
  end

  def display_version
    if self.version.nil? || self.version.empty?
      'Unspecified'
    else
      self.version
    end
  end

  def display_release
    self.release ? self.release.name : 'Unspecified'
  end

  def title=(value)
    if value
      words = value.split
      words.first.capitalize! if words.first && words.first !~ /^[A-Z]+$/
      value = words.join(' ')
    end
    write_attribute 'title', value
  end

  def send_assignment_email?
    self.changes.last.assignment_change? && self.user.is_notify?
  end

  def send_change_email?
    !self.subscriptions.empty?
  end

  ################################################################################
  protected
  ################################################################################

  ESTIMATOR = lambda{|v| v.gsub(/(\d+)d/){$1.to_i != 1 ? "#$1 Days " : "#$1 Day "}.
        gsub(/(\d+)h/){$1.to_i != 1 ? "#$1 Hours " : "#$1 Hour "}.
        gsub(/(\d+)m/){$1.to_i != 1 ? "#$1 Minutes " : "#$1 Minute "}
  }

  def before_create
    self.date_created = Time.now
    self.subscriptions ||= []
    self.comments_count = 0
    self.status = ACTIVE
  end

  validates_presence_of :title, :category, :user_id, :priority_id, :project_id

  def before_validation
    self.area = nil if self.area && self.project != self.area.project
  end

  def validate
    errors.add 'estimate', 'has invalid format' unless validate_estimate
  end

  def validate_estimate
    return true unless self.estimate
    return false if self.estimate !~ /^((\d+)d)?((\d+)h)?((\d+)m)?$/
    return false if $2 and $2.to_i == 0
    return false if $4 and $4.to_i == 0
    return false if $6 and $6.to_i == 0
    true
  end

  LOG_MAP = {
    'user_id' => ['Assigned', 'Unspecified', lambda{|v| User.find(v).full_name if v > 0}],
    'release_id' => ['Fix for', 'Unspecified', lambda{|v| Release.find(v).name if v > 0}],
    'priority_id' => ['Priority', 'Unspecified', lambda{|v| Priority.find(v).title if v > 0}],
    'project_id' => ['Project', 'Unspecified', lambda{|v| Project.find(v).name if v > 0}],
    'area_id' => ['Area', 'Unspecified', lambda{|v| Area.find(v).name if v > 0}],
    'estimate' => ['Estimate', 'Not Estimated', lambda{|v| ESTIMATOR[v] unless v.empty?}],
    'version' => ['Version', 'Unspecified', lambda{|v| v unless v.empty?}],
    'category' => ['Category', 'Unspecified', lambda{|v| v unless v.empty?}],
    'title' => ['Title', nil, lambda{|v| v unless v.empty?}],
  }

  def write_attribute(name, value)
    @log ||= {}

    if converter = LOG_MAP[name]
      old_value = read_attribute(name)
    
      column = column_for_attribute(name)
      if column
        value = column.type_cast(value) rescue value
      end

      loader = lambda{ |v|
        result = converter[2][v] if v
        result = converter[1] unless result
        result
      }

      if old_value != value
        @log[converter[0]] = [loader[old_value], loader[value]]
      end
    end

    super
  end

  private

  def is_open_change?(params)
    params['change_type'].to_i == Change::OPENED_TYPE
  end

  def is_assignment_change?(params)
    return false unless params['bug']['status'].to_i == ACTIVE
    return true if params['change_type'].to_i == Change::ASSIGNED_TYPE
    self.user_id && params['bug']['user_id'] && self.user_id != params['bug']['user_id'].to_i
  end
end
