class Priority < ActiveRecord::Base
  validates_presence_of :title

  def full_title
    "#{self.id}  -  #{self.title}"
  end
end
