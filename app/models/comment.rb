class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :bug, :counter_cache => true
  acts_as_tree :order => 'date_created', :counter_cache => true

  validates_presence_of :msg
  validates_format_of :msg, :with => /\w/, :on => :create

  def self.create(user_id, msg)
    new do |c|
      c.msg = msg
      c.user_id = user_id
      c.parent_id = nil
    end
  end

  def create_reply(user_id, msg)
    self.children.create('user_id' => user_id, 'msg' => msg, 'bug_id' => self.bug_id)
  end

  def display_date_created
    self.date_created.strftime('%m/%d/%Y %I:%M %p')
  end

  def user_name
    self.user.full_name
  end

  ######################################################################
  protected
  ######################################################################

  def before_create
    self.date_created = Time.now
    self.comments_count = 0
  end
end
