class UserController < ApplicationController

  model :user

  layout 'layouts/standard'

  before_filter :verify_access

  def list
    @users = User.find_all nil, 'login ASC'
  end

  def new; end

  def create
    params['user']['is_admin'] = 0 unless logged_user.is_admin?
    @user = User.new
    update_user
    if @user.save
      redirect_to :action => 'list'
    else
      render_action "new"
    end
  end

  def public
    flash['error'] = nil
    params['user']['status'] = User::ACTIVE
    @user = User.new
    update_user
    if @user.save
      params['login'] = @user.login
      params['pwd'] = @user.pwd
      login
    else
      render_action "login"
    end
  end

  def edit
    @user = User.find(params['id'])
  end

  def show
    edit
  end

  def update
    @user = User.find(params['user']['id'])
    update_user
    if @user.save
      flash['notice'] = 'User was successfully updated'
      redirect_to :action => 'show', :id => @user.id
    else
      render_action "edit"
    end
  end

  def login
    user = User.auth(params['login'], params['pwd'])
    if user
      session[USER_PARAM] = user
      set_filter(user)
    else
      flash['error'] = 'User is not found or password doesn\'t match.'
    end
    redirect_after_login
  end

  def logoff
    reset_session
    redirect_to :controller => 'bug', :action => 'list'
  end

  protected

  def redirect_after_login
    if session[RETURN_PARAM]
      redirect_to session[RETURN_PARAM]
      session[RETURN_PARAM] = nil
    else
      redirect_to :controller => 'bug', :action => 'list'
    end
  end
  
  def verify_access
    return true if %w{login logoff public}.include? action_name
    return true if logged_user.is_admin?
    if %w{edit show update}.include? action_name
      user_id = params['id'] || params['user']['id']
      if logged_user.id == user_id.to_i
        return true
      else
        access_denied
      end
    end
  end

  private

  def update_user
    user_params = params['user']
    if logged_user.nil? || !logged_user.is_admin?
      user_params.delete 'is_admin'
    end

    @user.attributes = user_params
    @user.pwd = params['new_pwd'] if params['new_pwd'] && !params['new_pwd'].empty?
  end
end
