class ApplicationController < ActionController::Base

  USER_PARAM = 'user'
  RETURN_PARAM = 'return_to'
  FILTER_PARAM = 'filter'

  before_filter :verify_login

  protected

  def logged_user
    session[USER_PARAM]
  end

  class FilterInfo
    attr_reader :filters, :current
    def initialize(user)
      @filters = user.filters.collect{|f| f.to_hash}
      @current = @filters.find{|fh| fh['id'] == user.filter_id}
    end

    def method_missing(name, *params)
      return nil unless @current
      return super unless @current.has_key? name.to_s
      @current[name.to_s]
    end
  end

  def set_filter(user = nil)
    user = User.find(logged_user.id) unless user
    session[FILTER_PARAM] = FilterInfo.new(user)
  end

  def verify_is_admin
    user = logged_user
    if user
      return true if user.is_admin?
      access_denied
    end
    true
  end

  def access_denied
    flash.now['error'] = 'Login with admin privileges.'
    render 'user/login'
    return false
  end

  private

  def verify_login
    if logged_user.nil? && !%w{login public}.include?(action_name)
      session[RETURN_PARAM] = @request.request_uri unless action_name == 'logoff'
      render 'user/login'
      return false
    end
    true
  end
end
