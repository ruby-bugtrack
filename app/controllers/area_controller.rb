class AreaController < ApplicationController

  model :project

  layout 'layouts/standard'

  before_filter :verify_is_admin

  def new
    @project = Project.find(params['id'])
  end

  def create
    @area = Area.new
    @area.attributes = params['area']
    if @area.save
      redirect_to :controller => 'project', :action => 'edit', :id => @area.project_id
    else
      @project = Project.find(params['project']['id'])
      render_action 'new'
    end
  end

  def edit
    @area = Area.find(params['id'])
  end

  def update
    @area = Area.find(params['area']['id'])
    @area.attributes = params['area']
    if @area.save
      redirect_to :controller => 'project', :action => 'edit', :id => @area.project_id
    else
      render_action 'edit'
    end    
  end

  def delete
    @area = Area.find(params['id'])
    Bug.update_all "area_id = NULL", "area_id = #{@area.id}"
    @area.destroy
    redirect_to :controller => 'project', :action => 'list'
  end
end
