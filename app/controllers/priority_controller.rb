class PriorityController < ApplicationController

  model :priority

  layout 'layouts/standard'

  before_filter :verify_is_admin

  def list
    @priorities = Priority.find_all(nil, 'id ASC')
  end

  def update
    list
    params['priority'].each do |id, value|
      priority = @priorities.find{|p| p.id == id.to_i}
      priority.attributes = value
      priority.save
    end
    flash['notice'] = 'Priorities were updated succesfully.'
    redirect_to :action => 'list'
  end

end
