class ReleaseController < ApplicationController

  model :project

  layout 'layouts/standard'

  before_filter :verify_is_admin

  def new
    @project = Project.find(params['id'])
  end

  def create
    @release = Release.new
    @release.attributes = params['release']
    if @release.save
      redirect_to :controller => "project", :action => "edit", :id => @release.project_id
    else
      @project = Project.find(params['project']['id'])
      render_action "new"
    end
  end

  def edit
    @release = Release.find(params['id'])
  end

  def update
    @release = Release.find(params['release']['id'])
    @release.attributes = params['release']
    if @release.save
      redirect_to :controller => "project", :action => "edit", :id => @release.project_id
    else
      render_action "edit"
    end
  end
end
