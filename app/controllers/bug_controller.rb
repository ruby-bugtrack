class BugController < ApplicationController

  model :bug, :change, :comment
  service :notifications

  layout 'layouts/standard'

  def index
    redirect_to :controller => 'bug', :action => 'list'
  end

  def list
    @sort_by = params['sort_by']
    @direction = params['direction']
    find_bugs{|p| Bug.find_all_for_listing(p, session[FILTER_PARAM], @sort_by, @direction)}
    @list_action = true
  end

  def search; end

  def find
    key, search_closed = params.values_at('key', 'search_closed')
    if params['full_text']
      find_bugs{|p| Bug.find_all_full_search(p, key, search_closed)}
    else
      find_bugs{|p| Bug.find_all_like_search(p, key, search_closed)}
    end
    if @bugs.empty?
      flash['notice'] = 'No tasks with specified criteria can be found.'
      render_action 'search'
    else
      render_action 'list'
    end
  end

  def new
    load_projects
    load_users
    load_properties
  end

  def show
    load_bug
    load_users
  end

  def edit
    @change_info = ChangeInfo.create(Change::EDITED_TYPE)
    load_bug
    render_edit
  end

  def resolve
    @change_info = ChangeInfo.create(Change::RESOLVED_TYPE)
    load_bug
    render_edit
  end
  
  def reactivate
    @change_info = ChangeInfo.create(Change::REACTIVATED_TYPE)
    load_bug
    render_edit
  end

  def close
    @change_info = ChangeInfo.create(Change::CLOSED_TYPE)
    load_bug
    render_edit
  end

  def reopen
    @change_info = ChangeInfo.create(Change::REOPENED_TYPE)
    load_bug
    render_edit
  end

  def assign
    return render_show if params['assign_to'].nil? || params['assign_to'] == '-1'

    @bug = Bug.find(params['id'])

    params['change_type'] = Change::ASSIGNED_TYPE
    params['bug'] = {'user_id' => params['assign_to']}

    if @bug.edit(logged_user, params )
      send_notifications
      redirect_to_view 'changes', @bug.id, nil
    else
      render_show
    end
  end

  def create
    @bug = Bug.new
    if change = @bug.edit(logged_user, params)
      send_notifications
      flash['fade_id'] = change.id
      anchor = 'fade' if change.has_message?
      redirect_to_view 'changes', @bug.id, anchor
    else
      new
      @msg = params['msg']
      render_action 'new'
    end
  end

  def update
    @bug = Bug.find(params['bug']['id'])
    if change = @bug.edit(logged_user, params)
      send_notifications
      flash['fade_id'] = change.id
      anchor = 'fade' if change.has_message?
      redirect_to_view 'changes', @bug.id, anchor
    else
      @change_info = ChangeInfo.create(params['change_type'].to_i)
      @msg = params['msg']
      render_edit
    end
  end

  def bulk
    @bugs = [Bug.find(params['bugs'])].flatten
    @change_type = 
      case params['bulk_func']
      when 'Assign'
        Change::ASSIGNED_TYPE
      when 'Resolve'
        Change::RESOLVED_TYPE
      when 'Reactivate'
        Change::REACTIVATED_TYPE
      when 'Close'
        Change::CLOSED_TYPE
      when 'Reopen'
        Change::REOPENED_TYPE
      end
    if @bugs.empty?
      redirect_to :action => 'list'
    elsif @bugs.size == 1 && @change_info = ChangeInfo.create(@change_type)
      load_bug @bugs.first.id
      render_edit
    else
      load_users if @change_type == Change::ASSIGNED_TYPE
    end
  rescue ActiveRecord::RecordNotFound
    redirect_to :action => 'list'
  end

  def do_bulk
    bugs = [Bug.find(params['bugs'])].flatten
    bugs.each do |bug|
      bug.edit logged_user, params
    end
    redirect_to :action => 'list'
  end

  def next
    bug = Bug.find_next(params['id'], session[FILTER_PARAM])
    if bug
      redirect_to :action => 'show', :id => bug.id
    else
      redirect_to :action => 'list'
    end
  end

  def prev
    bug = Bug.find_prev(params['id'], session[FILTER_PARAM])
    if bug
      redirect_to :action => 'show', :id => bug.id
    else
      redirect_to :action => 'list'
    end
  end

  def subscribe
    bug = Bug.find(params['id'])
    bug.subscribe logged_user.id
    bug.save
    redirect_to :action => 'show', :id => bug.id
  end

  def unsubscribe
    bug = Bug.find(params['id'])
    bug.unsubscribe logged_user.id
    bug.save
    redirect_to :action => 'show', :id => bug.id
  end

  def attachment
    change = Change.find(params['id'])
    send_file(change.attachment_path, :type => change.attachment_type)
  end

  def show_changes
    redirect_to_view 'changes'
  end

  def show_comments
    redirect_to_view 'comments'
  end

  def change_edit
    change = Change.find(params['id'])
    if logged_user.can_edit_change(change)
      change.msg = params['msg']
      if change.empty?
        change.destroy
        redirect_to_view 'changes', change.bug_id, 'history'
      else
        change.save
        flash['fade_id'] = change.id
        redirect_to_view 'changes', change.bug_id, 'fade'
      end
    end
  end

  def comment_create
    bug = Bug.find(params['id'])
    comment = bug.create_comment(logged_user.id, params['msg'])
    flash['fade_id'] = comment.id if comment
    redirect_to_view 'comments', comment.bug_id, 'fade'
  end

  def comment_reply
    comment = Comment.find(params['id'])
    new_comment = comment.create_reply(logged_user.id, params['msg'])
    flash['fade_id'] = new_comment.id if new_comment
    redirect_to_view 'comments', comment.bug_id, 'fade'
  end

  ######################################################################
  private
  ######################################################################

  def redirect_to_view(type, id = nil, anchor = 'history')
    id ||= params['id']
    session['view_type'] = type
    redirect_to :action => 'show', :id => id, :anchor => anchor
  end

  def find_bugs
    projects = Project.find_all('status = 1', 'name ASC')
    @bugs = projects.map{|p| [p, yield(p)]}
    @bugs.delete_if{|p, bugs| bugs.empty?}
    session['view_type'] = nil
  end

  def render_edit
    load_for_edit
    render_action 'edit'
  end

  def render_show
    show
    render_action 'show'
  end

  def load_for_edit
    load_projects
    load_users
    load_properties
  end

  def load_bug(id = nil)
    id ||= params['id']
    if id.nil? || id == ''
      redirect_to :action => 'list'
    else
      @bug = Bug.find(id)
    end
  rescue ActiveRecord::RecordNotFound
    flash['notice'] = "Task #{params['id']} is not found."
    redirect_to :action => 'search'
  end

  def load_users
    @users = User.find_all 'status = 1', 'first_name ASC'
  end

  def load_projects
    @projects = Project.find_all 'status = 1', 'id ASC'    
  end

  def load_properties
    @priorities = Priority.find_all nil, 'id ASC'
  end

  def send_notifications
    url = url_for(:action => 'show', :id => @bug.id)
    if @bug.send_assignment_email?
      Notifications.deliver_assignment(@bug, url)
    end
    if @bug.send_change_email?
      users = [User.find(@bug.subscriptions)].flatten
      users.delete_if{|user| !user.active? || !user.is_notify?}
      Notifications.deliver_change_log(@bug, users, url) unless users.empty?
    end
  end

  class ChangeInfo
    attr_reader :title, :status_map, :type

    def initialize(title, status_map, type)
      @title, @status_map, @type = title, status_map, type
    end

    def type_name
      Change::CHANGE_TYPES[type]
    end

    def self.create(type)
      case type
      when Change::EDITED_TYPE
        new 'Edit Task', [[Bug::ACTIVE, Bug::STATUS_NAMES[Bug::ACTIVE]]], type
      when Change::RESOLVED_TYPE
        new 'Resolve Task', Bug::RESOLVE_RANGE.map{|i| [i, Bug::STATUS_NAMES[i]]}, type
      when Change::REACTIVATED_TYPE
        new 'Reactivate Task', [[Bug::ACTIVE, Bug::STATUS_NAMES[Bug::ACTIVE] + " (Reactivate)"]], type
      when Change::CLOSED_TYPE
        new 'Close Task', [[Bug::CLOSED, Bug::STATUS_NAMES[Bug::CLOSED]]], type
      when Change::REOPENED_TYPE
        new 'Reopen Task', [[Bug::ACTIVE, Bug::STATUS_NAMES[Bug::ACTIVE] + " (Reopened)"]], type
      end
    end
  end
end
