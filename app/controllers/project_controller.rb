class ProjectController < ApplicationController

  model :project

  layout 'layouts/standard'

  before_filter :verify_is_admin

  def new
    load_users
  end

  def edit
    load_users
    @project = Project.find params['id']
  end

  def show
    @project = Project.find params['id']
  end

  def create
    @project = Project.new
    @project.attributes = params['project']
    if @project.save
      redirect_to :action => "list"
    else
      load_users
      render_action "new"
    end
  end

  def update
    @project = Project.find params['project']['id']
    @project.attributes = params['project']

    if @project.save
      flash['notice'] = 'Project was successfully updated'
      redirect_to :action => "list"
    else
      load_users
      render_action "edit"
    end
  end

  def list
    @projects = Project.find_all_with_manager
  end

  private

  def load_users
    @users = User.find_all 'status = 1', 'login ASC'
  end

  ######################################################################
  protected
  ######################################################################

  def verify_is_admin
    unless action_name == 'show'
      super 
    else
      true
    end
  end
end
