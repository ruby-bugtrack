class FilterController < ApplicationController

  model :project, :priority, :filter

  layout 'layouts/standard'

  def list
    @filters = Filter.find_all ['user_id = ?', logged_user.id], 'name'
  end

  def new
    @projects = Project.find_all nil, 'name ASC'
    @users = User.find_all nil, 'first_name ASC'
    @priorities = Priority.find_all nil, 'id ASC'
  end

  def create
    filter = Filter.new
    filter.setup(params['filter'])
    filter.user_id = logged_user.id
    if filter.save
      set_filter
      redirect_to :action => 'list'
    else
      setup_edit filter
      render_action 'new'
    end
  end

  def edit
    filter = find_filter
    setup_edit filter
  end

  def update
    filter = find_filter
    filter.setup(params['filter'])
    if filter.save
      set_filter
      redirect_to :action => 'list'
    else
      setup_edit filter
      render_action 'edit'
    end
  end

  def apply
    filter = params['id'] == 'all' ? nil : find_filter
    
    user = User.find(logged_user.id)
    user.filter = filter
    user.save
    
    set_filter(user)
    redirect_to :controller => 'bug', :action => 'list'
  end

  def delete
    filter = find_filter
    filter.destroy
    set_filter
    redirect_to :action => 'list'
  end

  private

  def setup_edit(filter)
    @filter = filter.filter
    @filter_id = filter.id
    @filter_obj = filter
    new
  end
  
  def find_filter
    Filter.find(params['id'])
  end
end
