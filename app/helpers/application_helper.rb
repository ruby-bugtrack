module ApplicationHelper
  def render_errors_for(mobj_name)
    mobj = instance_eval("@#{mobj_name}")
    return if mobj.nil? || mobj.errors.empty?
    result = "<div id=\"error\">"
    result << "<ul>"
    mobj.errors.each_full do |msg|
      result << "<li>#{msg}</li>"
    end
    result << "</ul>"
    result << "</div>"
  end

  def jsh(value)
    value.gsub(/['"]/){"\\" + $&} if value
  end

  def logged_user
    session[::ApplicationController::USER_PARAM]
  end

  def iterate_for_table(collection, class_names)
    collection.each_with_index do |item, row|
      yield item, class_names[row % 2]
    end
  end

  def fade_id(id)
    if flash['fade_id'] && flash['fade_id'] == id
      'id="fade"'
    end
  end

  def fade_run
    %{
      <script language="javascript" type="text/javascript" src="/javascripts/fade.js"></script>
      <script language="javascript" type="text/javascript">
        waittofade();
      </script>
    }
  end
end
