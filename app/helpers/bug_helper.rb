module BugHelper
  def sort_link_to(name, sort)
    if @list_action
      options = {}
      params = {'sort_by' => sort}
      if sort == @sort_by || (sort == 'id' && @sort_by.nil?)
        options['class'] = (@direction == 'DESC' ? 'sorted_desc' : 'sorted_asc')
        params['direction'] = case @direction
                              when 'ASC'
                                'DESC'
                              when 'DESC'
                                'ASC'
                              else
                                'DESC'
                              end
      end
      link_to name, {:action => 'list', :params => params}, options
    else
      name
    end
  end

  def show_changes_link(bug)
    html_options = {'title' => 'Display changes log'}
    if session['view_type'].nil? || session['view_type'] == 'changes'
      html_options['id'] = 'current'
    end
    link_to "Change History", {:action => "show_changes", :id => bug.id}, html_options
  end

  def show_comments_link(bug)
    html_options = {'title' => 'Display comments to this task'}
    html_options['id'] = 'current' if session['view_type'] == 'comments'
    link_to "Comments (#{bug.comments_count})", {:action => "show_comments", :id => bug.id}, html_options
  end

  def view_comments?
    session["view_type"] == "comments"
  end

  def status_class(bug)
    case bug.status
    when Bug::ACTIVE
      "bug_status_active"
    when Bug::RESOLVE_RANGE
      "bug_status_resolved"
    when Bug::CLOSED
      "bug_status_closed"
    end
  end

  def priority_class(bug)
    "bug_priority_#{bug.priority_id}"
  end

  def category_span(category)
    if category == "Bug"
      "<span class=\"bug\">#{category}</span>"
    elsif category == "Feature"
      "<span class=\"feature\">#{category}</span>"
    elsif category == "Inquiry"
      "<span class=\"inquiry\">#{category}</span>"
    else
      category
    end
  end
end
