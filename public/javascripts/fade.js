var Color= new Array();
Color[1] = "ff";
Color[2] = "ee";
Color[3] = "dd";
Color[4] = "cc";
Color[5] = "bb";
Color[6] = "aa";
Color[7] = "99";

var restoreColor = "transparent";

function waittofade() {
    var fade = document.getElementById('fade');
    if (fade) {
	restoreColor = fade.style.backgroundColor;
	setTimeout("fadeIn(7)", 1000);
    }
}

function fadeIn(where) {
    if (where >= 1) {
	var fade = document.getElementById('fade');
        fade.style.backgroundColor = "#EDF7" + Color[where];
	if (where > 1) {
	    where -= 1;
	    setTimeout("fadeIn("+where+")", 200);
	} else {
	    where -= 1;
	    setTimeout("fadeIn("+where+")", 200);
	    fade.style.backgroundColor = restoreColor;
	}
    }
}
