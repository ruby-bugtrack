function toggle(id) {
    var o = document.getElementById(id);
    o.style.display = (o.style.display == 'none') ? '' : 'none';
    return false;
}

function focusForm(id) {
    var f = document.getElementById(id);
    if(f.style.display != 'none')
        f['msg'].focus();
    return false;
}

function toggleForm(name) {
    toggle(name);
    focusForm(name);
    return false;
}

function toggleReply(id) {
    toggle('reply_btn_' + id);
    toggleForm('reply_' + id);
    toggle('comment_frm_toggle');
    return false;
}

function toggleEdit(id) {
    toggle('edit_btn_' + id);
    toggleForm('edit_' + id);
    return false;
}

function toggleComment() {
    toggle('comment_frm_toggle');
    toggleForm('comment_frm');
    return false;
}

function changeDelete(id) {
    var f = document.getElementById('edit_' + id);
    f.msg.value = '';
    f.submit();
}

function validateCommentForm(id) {
    var f = document.getElementById(id);
    if(f.msg.value.match(/^\s*$/)) {
	alert('Comment cannot be empty!');
	return false;
    }
    return true;
}

function checkPassword() {
    if(document.frm.new_pwd.value != document.frm.confirm_pwd.value) {
	alert("Passwords do not match!");
	document.frm.new_pwd.focus();
	return false;
    }
    return true;
}

function frmbugOnSubmit() {
    var id = document.frmbug['id'];
    if(!id.value.match(/^\d+$/)) {
	alert('Invalid number');
	id.focus();
	return false;
    }
    return true;
}

function frmbugDoSubmit() {
    if(frmbugOnSubmit())
	document.frmbug.submit();
}
