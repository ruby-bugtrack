require File.dirname(__FILE__) + '/../test_helper'
require 'release_controller'

# Raise errors beyond the default web-based presentation
class ReleaseController; def rescue_action(e) raise e end; end

class ReleaseControllerTest < Test::Unit::TestCase
  fixtures :projects, :releases, :users

  def setup
    @controller = ReleaseController.new
    @request = ActionController::TestRequest.new
    @response = ActionController::TestResponse.new
    @user_info = @kent
  end

  def test_new
    get :new, {'id' => 1}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'new'
    assert_template_has 'project'
  end

  def test_create
    post :create, {
      'release' => {'project_id' => 1, 'name' => 'New Release', 'assignable' => 1},
      'project' => {'id' => 1}
    }, {'user' => @user_info}

    assert_redirected_to :controller => 'project', :action => 'edit', :id => 1
  end

  def test_create_failed
    post :create, {
      'release' => {'project_id' => 1, 'name' => '', 'assignable' => 1},
      'project' => {'id' => 1}
    }, {'user' => @user_info}

    assert_success
    assert_rendered_file 'new'
    assert_template_has 'project'
  end

  def test_edit
    get :edit, {'id' => 1}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'edit'
    assert_template_has 'release'
  end

  def test_update
    post :update, {'release' => {'id' => 1, 'name' => 'New Name', 'assignable' => 0}}, {'user' => @user_info}
    assert_redirected_to :controller => 'project', :action => 'edit', :id => 1
  end

  def test_update_failed
    post :update, {'release' => {'id' => 1, 'name' => '', 'assignable' => 0}}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'edit'
    assert_template_has 'release'
  end
end
