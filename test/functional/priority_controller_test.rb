require File.dirname(__FILE__) + '/../test_helper'
require 'priority_controller'

# Raise errors beyond the default web-based presentation
class PriorityController; def rescue_action(e) raise e end; end

class PriorityControllerTest < Test::Unit::TestCase
  fixtures :priorities, :users

  def setup
    @controller = PriorityController.new
    @request = ActionController::TestRequest.new
    @response = ActionController::TestResponse.new
    @user_info = @kent
  end

  def test_list
    get :list, nil, {'user' => @user_info}
    assert_success
    assert_rendered_file 'list'
    assert_template_has 'priorities'
  end

  def test_update
    post :update, {'priority' => {1 => {'title' => 'New Title'}}}, {'user' => @user_info}
    assert_redirected_to :action => 'list'
    assert_flash_has 'notice'
    assert_equal 'New Title', @priorities['p_1'].find.title 
  end
end
