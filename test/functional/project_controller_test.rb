require File.dirname(__FILE__) + '/../test_helper'
require 'project_controller'

# Raise errors beyond the default web-based presentation
class ProjectController; def rescue_action(e) raise e end; end

class ProjectControllerTest < Test::Unit::TestCase
  fixtures :projects, :users

  def setup
    @controller = ProjectController.new
    @request = ActionController::TestRequest.new
    @response = ActionController::TestResponse.new
    @user_info = @kent
  end

  def test_new
    get :new, nil, {'user' => @user_info}
    assert_success 
    assert_rendered_file 'new'
    assert_template_has 'users'
  end

  def test_new_not_admin
    @user_info.is_admin = 0
    get :new, nil, {'user' => @user_info}
    assert_rendered_file 'login'
  end

  def test_create
    post :create, {
      'project' => {'name' => 'Ruby', 'manager_id' => 1, 'status' => 1}
    }, {'user' => @user_info}
    assert_redirected_to :action => 'list'
  end

  def test_create_failed
    post :create, nil, {'user' => @user_info}
    assert_success
    assert_rendered_file 'new'
    assert_template_has 'users'
  end

  def test_edit
    get :edit, {'id' => 1}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'edit'
    assert_template_has 'project'
  end

  def test_update
    post :update, {'project' => {'id' => 1, 'name' => 'Ruby'}}, {'user' => @user_info}
    assert_redirected_to :action => 'list'
    assert_flash_has 'notice'
  end

  def test_update_failed
    post :update, {'project' => {'id' => 1, 'name' => ''}}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'edit'
    assert_template_has 'users'
  end

  def test_list
    get :list, nil, {'user' => @user_info}
    assert_success 
    assert_rendered_file 'list'
    assert_template_has 'projects'
  end

  def test_show
    @user_info.is_admin = 0
    get :show, {'id' => 1}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'show'
  end
end
