require File.dirname(__FILE__) + '/../test_helper'
require 'area_controller'

# Raise errors beyond the default web-based presentation
class AreaController; def rescue_action(e) raise e end; end

class AreaControllerTest < Test::Unit::TestCase
  fixtures :users, :projects, :areas

  def setup
    @controller = AreaController.new
    @request = ActionController::TestRequest.new
    @response = ActionController::TestResponse.new
    @user_info = @kent
  end

  def test_new
    get :new, { 'id' => 1 }, { 'user' => @user_info }
    assert_success
    assert_rendered_file 'new'
    assert_template_has 'project'
  end

  def test_create
    post :create, {'area' => {'project_id' => 1, 'name' => 'New area'}}, {'user' => @user_info}
    assert_redirected_to :controller => 'project', :action => 'edit', :id => 1
  end

  def test_create_failed
    post :create, {
      'area' => {'project_id' => 1, 'name' => ''}, 
      'project' => {'id' => 1}
    }, {'user' => @user_info}
    assert_success
    assert_rendered_file 'new'
    assert_template_has 'project'
  end

  def test_edit
    get :edit, {'id' => 1}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'edit'
    assert_template_has 'area'
  end

  def test_update
    post :update, {'area' => {'id' => 1, 'name' => 'New Name'}}, {'user' => @user_info}
    assert_redirected_to :controller => 'project', :action => 'edit', :id => 1
  end

  def test_update_failed
    post :update, {'area' => {'id' => 1, 'name' => ''}}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'edit'
    assert_template_has 'area'
  end
end
