require File.dirname(__FILE__) + '/../test_helper'
require 'bug_controller'

# Raise errors beyond the default web-based presentation
class BugController; 
  attr_reader :sent_file
  def rescue_action(e) raise e end
  def send_file(path, options) 
    @sent_file = [path, options] 
    render_text 'sent'
  end
end

class BugControllerTest < Test::Unit::TestCase
  fixtures :bugs, :changes, :users

  def setup
    @controller = BugController.new
    @request = ActionController::TestRequest.new
    @response = ActionController::TestResponse.new
    @user_info = @kent
  end

  def test_index
    get :index, nil, {'user' => @user_info}
    assert_redirected_to :controller => 'bug', :action => 'list'
  end

  def test_search
    get :search, nil, {'user' => @user_info}
    assert_success
    assert_rendered_file 'search'
  end

  def test_list
    get :list, nil, {'user' => @user_info}
    assert_success
    assert_rendered_file 'list'
    assert_template_has 'bugs'
    assert_template_has 'list_action'
  end

  def test_find
    post :find, {'key' => 'Bug', 'search_closed' => true}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'list'
    assert_template_has 'bugs'
    assert_template_has_no 'list_action'
  end

  def test_find_failed
    post :find, {'key' => 'Dummy', 'search_closed' => true}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'search'
    assert_flash_has 'notice'
  end

  def test_new
    get :new, nil, {'user' => @user_info}
    assert_success
    assert_rendered_file 'new'
    assert_template_has 'projects'
    assert_template_has 'users'
    assert_template_has 'priorities'
  end

  def test_show
    get :show, {'id' => '1'}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'show'
    assert_template_has 'bug'
    assert_template_has 'users'
  end

  def test_show_not_found
    get :show, {'id' => '111'}, {'user' => @user_info}
    assert_redirected_to :action => 'search'
    assert_flash_has 'notice'
  end

  def test_show_no_id
    get :show, {'id' => ''}, {'user' => @user_info}
    assert_redirected_to :action => 'list'
  end
  
  %w{edit resolve reactivate close reopen}.each do |action|
    define_method("test_#{action}") do
      get action.intern, {'id' => '1'}, {'user' => @user_info}
      assert_success
      assert_rendered_file 'edit'
      assert_template_has 'bug'
      assert_template_has 'users'
      assert_template_has 'projects'
      assert_template_has 'priorities'
      assert_template_has 'change_info'
    end
  end

  def test_assign_no_param
    post :assign, {'id' => '1'}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'show'
    assert_template_has 'bug'

    post :assign, {'id' => '1', 'assign_to' => '-1'}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'show'
    assert_template_has 'bug'
  end

  def test_assign
    post :assign, {'id' => '1', 'assign_to' => '2'}, {'user' => @user_info}
    assert_redirected_to :action => 'show', :id => 1
    # TODO: send_notifications should be tested as well
  end

  def test_create
    post :create, {
      'bug' => {
        'id' => '1', 'title' => 'New Title', 
        'project_id' => 1, "category" => 'Bug', 
        'user_id' => 1, 'priority_id' => 1
      },
      'change_type' => 0,
      'msg' => 'New edit'
    }, {'user' => @user_info}
    assert_redirect
  end

  def test_create_failed
    post :create, {
      'bug' => {'id' => '1', 'title' => ''},
      'change_type' => 1,
      'msg' => 'New edit'
    }, {'user' => @user_info}
    assert_success
    assert_rendered_file 'new'
    assert_template_has 'projects'
    assert_template_has 'users'
    assert_template_has 'priorities'
    assert_template_has 'msg'
  end

  def test_update
    post :update, {'bug' => {'id' => 1, 'title' => 'Updated Title'}, 'change_type' => 1}, 
    {'user' => @user_info}
    assert_redirected_to :action => 'show', :id => 1

    post :update, {
      'bug' => {'id' => 1, 'title' => 'Updated Title'}, 
      'change_type' => 1, 
      'msg' => 'Edit message'
    }, {'user' => @user_info}
    assert_redirected_to :action => 'show', :id => 1, :anchor => 'fade'
  end

  def test_update_failed
    post :update, {'bug' => {'id' => 1, 'title' => ''}, 'change_type' => 1}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'edit'
    assert_template_has 'change_info'
  end

  def test_bulk_empty
    post :bulk, {'bugs' => [], 'bulk_func' => 'Assign'}, {'user' => @user_info}
    assert_redirected_to :action => 'list'
  end

  def test_bulk_only_one
    post :bulk, {'bugs' => [1], 'bulk_func' => 'Resolve'}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'edit'
    assert_template_has 'bug'
  end

  def test_bulk
    post :bulk, {'bugs' => [1, 2], 'bulk_func' => 'Resolve'}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'bulk'
    assert_template_has 'bugs'
    assert_template_has_no 'users'
  end

  def test_bulk_assign
    post :bulk, {'bugs' => [1, 2], 'bulk_func' => 'Assign'}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'bulk'
    assert_template_has 'bugs'
    assert_template_has 'users'
  end

  def test_do_bulk
    post :do_bulk, {'bugs' => [1, 2], 'bug' => {'status' => 1}, 'msg' => 'Bulk msg'}, {'user' => @user_info}
    assert_redirected_to :action => 'list'
  end

  def test_next
    get :next, {'id' => 1}, {'user' => @user_info}
    assert_redirected_to :action => 'show', :id => 2
  end

  def test_next_failed
    get :next, {'id' => 5}, {'user' => @user_info}
    assert_redirected_to :action => 'list'
  end

  def test_prev
    get :prev, {'id' => 2}, {'user' => @user_info}
    assert_redirected_to :action => 'show', :id => 1
  end

  def test_prev_failed
    get :prev, {'id' => 1}, {'user' => @user_info}
    assert_redirected_to :action => 'list'
  end

  def test_subscribe
    post :subscribe, {'id' => 1}, {'user' => @user_info}
    assert_redirected_to :action => 'show', :id => 1
  end

  def test_unsubscribe
    post :unsubscribe, {'id' => 1}, {'user' => @user_info}
    assert_redirected_to :action => 'show', :id => 1
  end

  def test_attachment
    post :attachment, {'id' => 1}, {'user' => @user_info}
    assert_equal [File.join($ATTACHMENTS, '1', 'test'), {:type => 'html/text'}], @controller.sent_file
  end

  def test_show_changes
    post :show_changes, {'id' => 1}, {'user' => @user_info}
#    assert_redirected_to :action => 'show', :id => 1, :anchor => 'history'
    assert_redirected_to :action => 'show', :anchor => 'history'
    assert_session_has 'view_type'
    assert_equal 'changes', @request.session['view_type']
  end

  def test_show_comments
    post :show_comments, {'id' => 1}, {'user' => @user_info}
#    assert_redirected_to :action => 'show', :id => 1, :anchor => 'history'
    assert_redirected_to :action => 'show', :anchor => 'history'
    assert_session_has 'view_type'
    assert_equal 'comments', @request.session['view_type']
  end

  def test_comment_create
    post :comment_create, {'id' => 1, 'msg' => 'new comment'}, {'user' => @user_info}
    assert_redirected_to :action => 'show', :id => 1, :anchor => 'fade'
  end

  def test_comment_reply
    post :comment_reply, {'id' => 1, 'msg' => 'new reply'}, {'user' => @user_info}
    assert_redirected_to :action => 'show', :id => 1, :anchor => 'fade'
  end

  def test_change_edit
    post :change_edit, {'id' => 2, 'msg' => 'new message'}, {'user' => @user_info}
    assert_redirected_to :action => 'show', :id => 1, :anchor => 'fade'
    assert_equal 'new message', Change.find(2).msg
  end

  def test_change_edit_empty
    post :change_edit, {'id' => 2, 'msg' => ''}, {'user' => @user_info}
    assert_redirected_to :action => 'show', :id => 1, :anchor => 'history'
    assert_raise(ActiveRecord::RecordNotFound) { Change.find 2 }
  end
end
