require File.dirname(__FILE__) + '/../test_helper'
require 'filter_controller'

# Raise errors beyond the default web-based presentation
class FilterController; def rescue_action(e) raise e end; end

class FilterControllerTest < Test::Unit::TestCase
  fixtures :users, :filters

  def setup
    @controller = FilterController.new
    @request, @response = ActionController::TestRequest.new, ActionController::TestResponse.new
    @user_info = @kent
  end

  def test_list
    get :list, nil, {'user' => @user_info}
    assert_success
    assert_rendered_file 'list'
    assert_template_has 'filters'
  end

  def test_new
    get :new, nil, {'user' => @user_info}
    assert_success
    assert_rendered_file 'new'
    assert_template_has 'projects'
    assert_template_has 'users'
    assert_template_has 'priorities'
  end

  def test_create
    filter = create_filter{|f| f.name = 'New Filter'}
    post :create, {'filter' => filter }, {'user' => @user_info}
    assert_redirected_to :action => 'list'
    assert_filter 3, 1
  end

  def test_create_failed
    filter = create_filter{|f| f.name = ''}
    post :create, {'filter' => filter}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'new'
    assert_template_has 'projects'
    assert_template_has 'users'
    assert_template_has 'priorities'
    assert_template_has 'filter'
    assert_template_has 'filter_obj'
    assert_session_has_no 'filter'
  end

  def test_edit
    get :edit, {'id' => 1}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'edit'

    assert_template_has 'projects'
    assert_template_has 'users'
    assert_template_has 'priorities'
    assert_template_has 'filter'
    assert_template_has 'filter_id'
    assert_template_has 'filter_obj'
  end

  def test_update
    filter = create_filter{|f| f.name = "new"}
    post :update, {'id' => 1, 'filter' => filter}, {'user' => @user_info}
    assert_redirected_to :action => 'list'
    assert_filter 2, 1
  end

  def test_update_failed
    filter = create_filter{|f| f.name = 'Another filter'}
    post :update, {'id' => 1, 'filter' => filter}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'edit'

    assert_template_has 'projects'
    assert_template_has 'users'
    assert_template_has 'priorities'
    assert_template_has 'filter'
    assert_template_has 'filter_id'
    assert_template_has 'filter_obj'

    assert_session_has_no 'filter'
  end

  def test_apply
    post :apply, {'id' => 2}, {'user' => @user_info}
    assert_redirected_to :controller => 'bug', :action => 'list'
    assert_equal 2, User.find(1).filter_id
    assert_filter 2, 2
  end

  private

  def assert_filter(size, cur_id)
    assert_session_has 'filter'
    filter = @request.session['filter']
    assert_equal size, filter.filters.size
    assert_equal cur_id, filter.current['id']
  end

  def create_filter
    filter = Filter.default_filter(1)
    yield filter if block_given?
    filter.send(:table).inject({}){ |h, (k,v)| 
      if v.kind_of?(Array)
        h[k] = v
      else
        h[k] = v ? v.to_s : ''
      end
      h
    }
  end
end
