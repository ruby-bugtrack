require File.dirname(__FILE__) + '/../test_helper'

class FilterInfoTest < Test::Unit::TestCase
  FilterInfo = ApplicationController::FilterInfo

  def setup
    @user = Class.new do
      attr_accessor :filter_id
      def filters
        [
          {'id' => 1, 'key1' => 'f1_value1', 'key2' => 'f1_value2'},
          {'id' => 2, 'key1' => 'f2_value1', 'key2' => 'f2_value2'}
        ]
      end
    end.new
  end

  def test_info
    @user.filter_id = 1
    filter_info = FilterInfo.new(@user)
    assert_equal 1, filter_info.current['id']
    assert_equal 'f1_value1', filter_info.key1
    assert_equal 'f1_value2', filter_info.key2

    @user.filter_id = 2
    filter_info = FilterInfo.new(@user)
    assert_equal 2, filter_info.current['id']
    assert_equal 'f2_value1', filter_info.key1
    assert_equal 'f2_value2', filter_info.key2
  end
end
