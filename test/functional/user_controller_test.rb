require File.dirname(__FILE__) + '/../test_helper'
require 'user_controller'

# Raise errors beyond the default web-based presentation
class UserController; def rescue_action(e) raise e end; end

class UserControllerTest < Test::Unit::TestCase
  fixtures :users

  def setup
    @controller = UserController.new
    @request = ActionController::TestRequest.new
    @response = ActionController::TestResponse.new
    @user_info = @kent
  end

  def test_list
    get :list
    assert_success
    assert_rendered_file 'login'
    
    get :list, nil, 'user' => @user_info
    assert_success
    assert_rendered_file 'list'
    assert_template_has 'users'
  end

  def test_create
    post :create, { 
      'user' => {
        'login' => 'new', 'first_name' => 'Demo', 'last_name' => 'User',
        'email' => 'demo@user.com', 'status' => '1',
      },
      'new_pwd' => '123'
    }, { 'user' => @user_info }
    assert_redirected_to :action => 'list'
  end

  def test_public
    post :public, { 
      'user' => {
        'login' => 'new', 'first_name' => 'Demo', 'last_name' => 'User',
        'email' => 'demo@user.com', 'status' => '1', 'is_admin' => '1',
      },
      'new_pwd' => '123'
    }
    assert_session_has 'user'
    assert !@request.session['user'].is_admin?
    assert_redirected_to :action => 'list'
  end

  def test_create_with_no_password
    post :create, { 
      'user' => {
        'login' => 'new', 'first_name' => 'Demo', 'last_name' => 'User',
        'email' => 'demo@user.com', 'status' => '1',
      },
    }, { 'user' => @user_info }
    assert_success
    assert_rendered_file 'new'
  end

  def test_edit
    post :edit, {'id' => 1}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'edit'
    assert_template_has 'user'
  end

  def test_show
    get :show, {'id' => 1}, {'user' => @user_info}
    assert_success
    assert_rendered_file 'show'
    assert_template_has 'user'
  end

  def test_update
    post :update, {'user' => {'id' => 1, 'first_name' => 'someone'}}, {'user' => @user_info}
    assert_redirected_to :action => 'show'
    assert_flash_has 'notice'
  end

  def test_update_another_user
    post :update, {'user' => {'id' => 2, 'first_name' => 'someone'}}, {'user' => @user_info}
    assert_redirected_to :action => 'show'
    assert_flash_has 'notice'

    @user_info.is_admin = 0
    post :update, {'user' => {'id' => 2, 'first_name' => 'someone'}}, {'user' => @user_info}
    assert_rendered_file 'login'
    assert_tag :tag => "div", :child => /Login with admin privileges/
  end

  def test_login
    post :login, 'login' => 'kent', 'pwd' => 'mypass'
    assert_redirected_to :controller => 'bug', :action => 'list'
    assert_session_has 'user'
    assert_flash_has_no 'error'
  end

  def test_login_with_return
    post :login, {'login' => 'kent', 'pwd' => 'mypass'}, {'return_to' => '/user/list'}
    assert_redirect_url_match 'user/list$'
    assert_session_has 'user'
    assert_flash_has_no 'error'
  end

  def test_login_failure
    post :login, 'login' => 'kent', 'pwd' => 'wrongpass'
    assert_flash_has 'error'
    assert_redirected_to :controller => 'bug', :action => 'list'
  end

  def test_logoff
    post :logoff, nil, {'user' => @user_info}
    assert_redirected_to :controller => 'bug', :action => 'list'
    assert_session_has_no 'user'
  end
end
