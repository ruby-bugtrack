require File.dirname(__FILE__) + '/../test_helper'

class CommentTest < Test::Unit::TestCase
  fixtures :comments, :bugs, :users

  def test_user_name
    assert_equal 'Kent Sibilev', @comment_1.user_name
    assert_equal 'Demo User', @comment_2.user_name
  end

  def test_association
    assert_equal 2, @bug_1.root_comments.size
    assert_equal 2, @bug_1.root_comments.first.children.size
    assert_equal 2, @comment_1.comments_count
    assert_equal 0, @comment_2.comments_count
  end

  def test_create_root
    assert @bug_1.create_comment(1, 'New comment')
    bug = @bugs['bug_1'].find
    assert_equal 5, bug.comments_count
    bug.root_comments.each do |comment|
      assert_nil comment.parent_id
    end
  end

  def test_create_reply
    comment = @bug_1.root_comments.last
    assert_equal 4, @bug_1.comments_count
    assert comment.children.empty?
    assert_equal 0, comment.comments_count

    new_comment = comment.create_reply(1, "New reply")
    assert new_comment
    assert new_comment.date_created
    assert_equal 0, new_comment.comments_count

    bug = @bugs['bug_1'].find
    assert_equal 5, bug.comments_count

    comment = bug.root_comments.last
    assert_equal 1, comment.comments_count
    assert_equal 1, comment.children.size
  end

  def test_validation
    assert !@bug_1.create_comment(1, nil)
  end
end
