require File.dirname(__FILE__) + '/../test_helper'

class BugTest < Test::Unit::TestCase
  fixtures(:priorities, :users, :projects, :bugs, :changes)

  def test_find_all_for_listing
    bugs = Bug.find_all_for_listing(@k_prj)
    
    assert_equal 4, bugs.size

    b1 = bugs.find{|b| b.id == 1}
    assert_equal 'Kent Sibilev', b1.user_name
    
    b2 = bugs.find{|b| b.id == 2}
    assert_equal 'Demo User', b2.user_name

    # make sure ordering was by id
    assert_same bugs[0], b1
    assert_same bugs[1], b2
  end

  def test_find_all_like_search
    bugs = Bug.find_all_like_search(@k_prj, "Title")
    assert_equal 1, bugs.size
    assert_equal 'Search For This Title', bugs[0].title
  end

  def test_find_next
    assert_equal @bug_2.id, Bug.find_next(1).id
  end

  def test_find_prev
    assert_equal @bug_1.id, Bug.find_prev(2).id
  end

  def test_open_change
    @my_bug.edit(@kent, 'change_type' => 0, 'msg' => 'change', 'bug' => {})
    assert_equal 1, @my_bug.changes.size
    chg = @my_bug.changes[0]
    assert_equal 'Opened', chg.type_name
    assert_nil chg.log
    assert_equal 'change', chg.msg
  end

  def test_edit_change
    chg = @my_bug.edit(@kent, 'change_type' => 1, 'msg' => 'edit', 
                 'bug' => {'title' => 'New Title', 'category' => 'Feature'})
    assert chg
    assert_equal 1, @bugs['my_bug'].find.changes.size
    assert_equal 'New Title', @my_bug.title
    assert_equal 'Feature', @my_bug.category
    assert_equal 1, @my_bug.changes.size
    assert_equal 'Edited', chg.type_name
    assert_not_nil chg.log
    assert_equal 2, chg.log.size
    assert_equal ['My Bug', 'New Title'], chg.log['Title']
    assert_equal ['Bug', 'Feature'], chg.log['Category']
  end

  def test_access_permission
    assert !@bug_1.edit(@demo, 'change_type' => 1, 'msg' => 'edit', 
                 'bug' => {'title' => 'New Title', 'category' => 'Feature'})
    assert !@demo2_bug.edit(@demo, 'change_type' => 1, 'msg' => 'edit', 
                 'bug' => {'title' => 'New Title', 'category' => 'Feature'})
    assert @demo2_bug.edit(@demo2, 'change_type' => 1, 'msg' => 'edit', 
                 'bug' => {'title' => 'New Title', 'category' => 'Feature'})
  end

  def test_edit_no_changes
    assert @my_bug.changes.empty?
    assert !@my_bug.edit(@kent, 'change_type' => 1, 'msg' => nil, 'bug' => {})
  end

  def test_assignment_change
    @my_bug.edit(@kent, 'change_type' => 1, 'msg' => 'edit', 
                 'bug' => {'title' => 'New Title', 'user_id' => 1})
    assert_equal 1, @my_bug.changes.size
    chg = @my_bug.changes[0]
    assert_equal 'Assigned', chg.type_name
    assert_not_nil chg.log
    assert_equal 2, chg.log.size
    assert_equal ['My Bug', 'New Title'], chg.log['Title']
    assert_equal ['Demo User', 'Kent Sibilev'], chg.log['Assigned']
    assert @my_bug.send_assignment_email?
  end

  def test_resolve_change
    @my_bug.edit(@kent, 'change_type' => 2, 'msg' => 'resolved', 'bug' => {'status' => 2})
    assert_equal 1, @my_bug.changes.size
    chg = @my_bug.changes[0]
    assert_equal 'Resolved', chg.type_name
    assert @my_bug.resolved?
  end

  def test_validate_presence
    @my_bug.title = nil
    assert !@my_bug.valid?
  end

  def test_validate_estimate
    @my_bug.estimate = '1dfe'
    assert !@my_bug.valid?

    @my_bug.estimate = '1d0h'
    assert !@my_bug.valid?

    @my_bug.estimate = '1d1h'
    assert @my_bug.valid?
  end

  def test_display_estimate
    @my_bug.estimate = '1d'
    assert '1 Day', @my_bug.display_estimate

    @my_bug.estimate = '2d'
    assert '2 Days', @my_bug.display_estimate

    @my_bug.estimate = '2d1h'
    assert '2 Days 1 Hour', @my_bug.display_estimate
  end

  def test_title
    @my_bug.title = 'my title'
    assert_equal 'My title', @my_bug.title
    
    @my_bug.title = 'MY title'
    assert_equal 'MY title', @my_bug.title
  end

  def test_subscribtion
    @my_bug.subscribe 1
    assert @my_bug.send_change_email?
    @my_bug.unsubscribe 1
    assert !@my_bug.send_change_email?
  end

  def test_full_changes
    changes = @bug_1.full_changes
    assert_equal 2, changes.size
    chg = changes.find{|c| c.id == 1}
    assert_equal 'Kent Sibilev', chg.user_name

    chg = changes.find{|c| c.id == 2}
    assert_equal 'Demo User', chg.user_name
  end

  def test_create
    bug = Bug.new
    assert bug.edit(@kent, 'change_type' => 0, 'msg' => 'edit', 
                    'bug' => {
                      'title' => 'New Bug Report', 
                      'category' => 'Feature',
                      'project_id' => 1,
                      'priority_id' => 1,
                      'user_id' => 1,
                    })
    bug = Bug.find bug.id
    assert_equal [1], bug.subscriptions
    assert bug.date_created
    assert_equal 0, bug.comments_count
    assert_equal 1, bug.open_user_id
  end
end
