require File.dirname(__FILE__) + '/../test_helper'

class ReleaseTest < Test::Unit::TestCase
  fixtures :releases, :projects

  def test_assignable
    assert @release.assignable?
    assert !@another.assignable?
  end

  def test_validation
    @release.name = nil
    assert !@release.valid?

    @release.name = 'Another'
    assert !@release.valid?
    
    @release.name = 'Yet Another'
    assert @release.valid?

    release = Release.new do |r|
      r.name = 'Release'
      r.project_id = 2
    end
    assert release.valid?
  end
end
