require File.dirname(__FILE__) + '/../test_helper'

class PriorityTest < Test::Unit::TestCase
  fixtures :priorities

  def test_full_title
    assert_equal "1  -  High", @p_1.full_title
  end

  def test_validation
    @p_1.title = nil
    assert !@p_1.valid?

    @p_2.title = ''
    assert !@p_2.valid?
  end
end
