require File.dirname(__FILE__) + '/../test_helper'

class ChangeTest < Test::Unit::TestCase
  fixtures :bugs, :changes, :users

  def test_extended_changes
    changes = Change.extended_changes(@bug_1)
    assert_equal 2, changes.size
    change = changes.find{|c| c.id == 1}
    assert_equal 'Kent Sibilev', change.user_name
    change = changes.find{|c| c.id == 2}
    assert_equal 'Demo User', change.user_name

    assert_equal 1, changes[0].id
    assert_equal 2, changes[1].id
  end

  def test_empty
    change = Change.new do |c|
      c.msg = 'message'
      c.log = {'Category' => %w{Bug Feature}}
      c.attachment = 'file.txt'
      c.change_type = Change::EDITED_TYPE
    end
    assert !change.empty?
    change.msg = nil
    assert !change.empty?
    change.log = {}
    assert !change.empty?
    change.attachment = nil
    assert change.empty?
    change.change_type = Change::OPENED_TYPE
    assert !change.empty?
  end

  def test_each_log
    change = Change.new do |c|
      c.change_type = Change::OPENED_TYPE
      c.log = {'Category' => %w{Bug Feature}, 'User' => %w{One Another}}
    end
    result = ""
    change.each_log{|c| result << c}
    assert result.empty?
    assert_nil change.log
    
    change = Change.new do |c|
      c.change_type = Change::EDITED_TYPE
      c.log = {'Category' => %w{Bug Feature}, 'User' => %w{One Another}}
    end
    change.each_log{|c| result << c << '!'}
    assert_equal 'Category from Bug to Feature!User from One to Another!', result
  end
end
