require File.dirname(__FILE__) + '/../test_helper'

class FilterTest < Test::Unit::TestCase
  def setup
    @filter = Filter.new
  end

  def test_include
    @filter.setup('name' => 'filter', 'include' => 'open')
    assert_equal 'bugs.status <> 1', @filter.sql

    @filter.setup('name' => 'filter', 'include' => 'closed')
    assert_equal 'bugs.status = 1', @filter.sql
  end

  def test_category
    @filter.setup('name' => 'filter', 'category' => 'Bug')
    assert_equal 'bugs.category = \'Bug\'', @filter.sql
  end

  def test_project
    @filter.setup('name' => 'filter', 'project' => 1)
    assert_equal 'bugs.project_id = 1', @filter.sql
  end
end
