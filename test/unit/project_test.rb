require File.dirname(__FILE__) + '/../test_helper'

class ProjectTest < Test::Unit::TestCase
  fixtures :projects, :users

  def test_find_all_with_manager
    projects = Project.find_all_with_manager

    k_prj = projects.find{|p| p.id == 1}
    assert_equal 'kent', k_prj.manager_name

    d_prj = projects.find{|p| p.id == 2}
    assert_equal 'demo', d_prj.manager_name
  end

  def test_validate
    @k_prj.name = nil
    assert !@k_prj.valid?

    @d_prj.name = 'Kent Project'
    assert !@d_prj.valid?
  end
end
