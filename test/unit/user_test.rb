require File.dirname(__FILE__) + '/../test_helper'

class UserTest < Test::Unit::TestCase
  fixtures :users

  def test_fullname
    assert_equal 'Kent Sibilev', @kent.full_name
  end

  def test_auth
    assert_not_nil User.auth('kent', 'mypass')
    assert_nil User.auth('kent', 'wrongpass')
    assert_nil User.auth('disabled', '123')
  end

  def test_attributes
    assert @kent.is_admin?
    assert !@demo.is_admin?
    assert @kent.is_notify?
    assert !@demo.is_notify?
    assert @kent.active?
    assert !@disabled.active?
  end

  ['login', 'first_name', 'last_name', 'email'].each do |name|
    eval %{
      def test_validate_presence_#{name}
        @kent.#{name} = nil
        assert !@kent.valid?
      end
    }
  end

  def test_validate_presence_of_password
    user = User.new do |u|
      u.login = 'new'
      u.first_name = 'f'
      u.last_name = 'l'
      u.email = 'email@name.com'
    end
    assert !user.valid?

    user = User.new do |u|
      u.login = 'new'
      u.first_name = 'f'
      u.last_name = 'l'
      u.email = 'email@name.com'
      u.pwd = ''
    end
    assert !user.valid?
  end

  def test_validate_uniqueness_of_login
    user = User.new do |u|
      u.login = 'kent'
      u.first_name = 'f'
      u.last_name = 'l'
      u.email = 'e'
      u.pwd = '123'
    end
    assert !user.valid?

    assert @demo.valid?
    @demo.login = 'kent'
    assert !@demo.valid?

  end
end
