require File.dirname(__FILE__) + '/../test_helper'

class AreaTest < Test::Unit::TestCase
  fixtures :projects, :areas

  def test_validate_presence
    @area.name = nil
    assert !@area.valid?
  end

  def test_validate_uniqueness
    area = Area.new do |a|
      a.name = 'Area'
      a.project_id = 1
    end
    assert !area.valid?

    area = Area.new do |a|
      a.name = 'Area 2'
      a.project_id = 1
    end
    assert area.valid?

    @area.name = 'Another Area'
    assert !@area.valid?
  end
end
